lazy val root = (project in file(".")).settings(
  name := "yeet",
  version := "0.1",
  scalaVersion := "2.12.5",
  libraryDependencies ++= Seq(
    "com.lihaoyi" %% "fastparse" % "1.0.0"
  ),
)
