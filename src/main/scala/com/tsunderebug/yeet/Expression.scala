package com.tsunderebug.yeet

trait Expression {

  def simplify: Expression
  def latex: String

}
