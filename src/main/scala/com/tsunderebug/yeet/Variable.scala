package com.tsunderebug.yeet

case class Variable(name: Char) extends Expression {

  override def simplify: Expression = this
  override def latex: String = name.toString

}
