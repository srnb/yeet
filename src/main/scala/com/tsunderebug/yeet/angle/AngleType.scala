package com.tsunderebug.yeet.angle

import com.tsunderebug.yeet
import com.tsunderebug.yeet.number.RealNumber

object AngleType {
  object Radians extends AngleType {
    override def radians(v: RealNumber): RealNumber = v
    override def degrees(v: RealNumber): RealNumber = v * 180.0 / yeet.pi
    override def gradians(v: RealNumber): RealNumber = v * 200.0 / yeet.pi
  }
  object Degrees extends AngleType {
    override def radians(v: RealNumber): RealNumber = v * yeet.pi / 180.0
    override def degrees(v: RealNumber): RealNumber = v
    override def gradians(v: RealNumber): RealNumber = v * 10.0 / 9.0
  }
  object Gradians extends AngleType {
    override def radians(v: RealNumber): RealNumber = v * yeet.pi / 200.0
    override def degrees(v: RealNumber): RealNumber = v * 9.0 / 10.0
    override def gradians(v: RealNumber): RealNumber = v
  }
}

sealed trait AngleType {
  def radians(v: RealNumber): RealNumber
  def degrees(v: RealNumber): RealNumber
  def gradians(v: RealNumber): RealNumber
  def ->(t: AngleType): (RealNumber) => RealNumber = {
    t match {
      case AngleType.Radians => radians
      case AngleType.Degrees => degrees
      case AngleType.Gradians => gradians
    }
  }
}
