package com.tsunderebug.yeet.calculus

import com.tsunderebug.yeet

trait Derivable[F <: yeet.function.Function] {

  def derive: F

}
