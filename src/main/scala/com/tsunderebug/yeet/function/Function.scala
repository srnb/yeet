package com.tsunderebug.yeet.function

import com.tsunderebug.yeet

trait Function {

  type D <: yeet.number.Number

  def isDifferentiableAt(n: D): Boolean
  def isDerivableOver[D](s: yeet.set.Set[D]): Boolean

  def isIntegratableOver[D](s: yeet.set.Set[D]): Boolean
  
}
