package com.tsunderebug.yeet.function

import com.tsunderebug.yeet.Expression
import com.tsunderebug.yeet.number.Number

trait PiecewiseConstraint[N <: Number] extends Expression {

  def isDefinedAt(n: N): Boolean
  def isContinuousAt(n: N): Boolean

}
