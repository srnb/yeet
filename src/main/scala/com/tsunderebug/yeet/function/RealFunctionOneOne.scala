package com.tsunderebug.yeet.function

import com.tsunderebug.yeet.number.RealNumber
import com.tsunderebug.yeet.{Expression, MathEnvironment, Variable, set}

case class RealFunctionOneOne(name: Char, input: Variable, output: Expression)(implicit mathEnvironment: MathEnvironment) extends RealFunction {

  override type D = RealNumber

  override def isDifferentiableAt(n: D): Boolean = output match {
    case pc: PiecewiseConstraint[RealNumber] => pc.isContinuousAt(n)
    case _ => false // TODO
  }

  override def isDerivableOver[D](s: set.Set[D]): Boolean = ???

  override def isIntegratableOver[D](s: set.Set[D]): Boolean = ???

}
