package com.tsunderebug.yeet.function

import com.tsunderebug.yeet.Expression
import com.tsunderebug.yeet.number.RealNumber
import com.tsunderebug.yeet.set.RealRange

case class RealPiecewiseConstraint(pieces: Map[RealRange, Expression]) extends PiecewiseConstraint[RealNumber] {

  override def simplify: Expression = RealPiecewiseConstraint(pieces.mapValues(_.simplify))

  override def latex: String = ???

  override def isDefinedAt(n: RealNumber): Boolean = pieces.keys.exists(_.isDefinedAt(n))

  override def isContinuousAt(n: RealNumber): Boolean = pieces.keys.forall(_.isContinuousAt(n))

}
