package com.tsunderebug.yeet.number

import com.tsunderebug.yeet.Expression

trait Number extends Expression {

  override def simplify: Expression = this

}
