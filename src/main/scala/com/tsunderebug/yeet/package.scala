package com.tsunderebug

import com.tsunderebug.yeet.number._

package object yeet {

  val pi: Double = Math.PI

  implicit def realNumberToDouble(rn: RealNumber): Double = rn.real
  implicit def doubleToRealNumber(d: Double): RealNumber = new RealNumber(d)

}
