package com.tsunderebug.yeet.set

import com.tsunderebug.yeet
import com.tsunderebug.yeet.number.RealNumber

case class RealRange(left: (RealNumber, Boolean), right: (RealNumber, Boolean)) extends Set[RealNumber] {

  override def isDefinedAt(n: RealNumber): Boolean = {
    (left, right) match {
      case ((l, true), (r, true)) => (l <= n) && (n <= r)
      case ((l, false), (r, true)) => (l < n) && (n <= r)
      case ((l, true), (r, false)) => (l <= n) && (n < r)
      case ((l, false), (r, false)) => (l < n) && (n < r)
    }
  }

  override def isContinuousAt(n: RealNumber): Boolean = {
    ((left, right) match {
      case ((l, true), (r, true)) => (l != n) && (r != n)
      case ((_, false), (r, true)) => r != n
      case ((l, true), (_, false)) => l != n
      case _ => true
    }) && isDefinedAt(n)
  }

}
