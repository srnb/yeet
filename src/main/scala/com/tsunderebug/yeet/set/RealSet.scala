package com.tsunderebug.yeet.set

import com.tsunderebug.yeet.number.RealNumber

object RealSet extends Set[RealNumber] {
  override def isDefinedAt(n: RealNumber): Boolean = true
  override def isContinuousAt(n: RealNumber): Boolean = true
}
