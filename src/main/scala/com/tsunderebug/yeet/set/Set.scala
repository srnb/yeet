package com.tsunderebug.yeet.set

import com.tsunderebug.yeet.number

trait Set[N <: number.Number] {

  def isDefinedAt(n: N): Boolean

  def isContinuousAt(n: N): Boolean

}
